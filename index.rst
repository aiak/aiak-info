.. index::
   ! AIAK
   ! Association Iseroise des Amis des Kurdes


.. raw:: html

   <a rel="me" href="https://kolektiva.social/@iranluttes"></a>
   <a rel="me" href="https://kolektiva.social/@kurdistanluttes"></a>


|FluxWeb| `RSS <https://aiak.frama.io/aiak-info/rss.xml>`_

.. _aiak:

==================================================================================================================
**AIAK, Association Iseroise des Amis des Kurdes**
==================================================================================================================

- https://www.facebook.com/people/Aiak/100064705085192/

::

    16 avenue du 8 mai 1945
    38 400 St Martin d'Hères
    tel : 06 86 89 54 95


`Maryvonne Mathéoud <https://travailleur-alpin.fr/author/maryvonne-matheoud/>`_, présidente d’AIAK


.. figure:: images/aiak_logo.png
   :align: center


Courriel: aiak.contact @ gmail.com

.. toctree::
   :maxdepth: 5

   articles/articles
