.. index::
   pair: AIAK ; 2025-01-09

.. _aiak_2025_01_09:

==========================================================================================================================================================================================================================================
2025-01-09 **Il y a douze ans, le 9 janvier 2013, les militantes kurdes Sakine Cansiz, Fidan Dogan et Leyla Saylemez étaient assassinées à Paris** : rassemblement le jeudi 9 janvier 2025 à 18h30 place Félix Poulat Grenoble |aiak|
==========================================================================================================================================================================================================================================

- :ref:`assassinats_2013_01_09`

:download:`Tract AIK en français et kurde <pdfs/rassemblement_aiak_2025_01_09.pdf>`


Lieu: rue Félix Poulat
=========================

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.724166631698609%2C45.189059881845104%2C5.727707147598267%2C45.19066475753779&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.189862/5.725937">Afficher une carte plus grande</a></small>

Il y a douze ans, le 9 janvier 2013, les militantes kurdes Sakine Cansiz, Fidan Dogan et Leyla Saylemez étaient assassinées à Paris
======================================================================================================================================

L'assassin présumé est décédé sans procès, mais différents éléments indiquent 
que le service de renseignements turc, le MIT, est impliqué dans cet assassinat.

Grâce à la persévérance des familles des trois victimes, le dossier du triple 
meurtre a pu être réouvert en 2019 pour "complicité d’assassinats en relation 
avec une entreprise terroriste et d’association de malfaiteurs terroriste criminelle ». 

La justice française a du mal à avancer, des pièces essentielles étant classées 
"secret défense ».

Nous réclamons 
------------------

- la levée du secret défense dans l’enquête de ces assassinats politiques 
  pour **pouvoir faire la pleine lumière** sur ces assassinats 
- **l’arrêt des mesures d’intimidation et de répression** contre les militant.es 
  kurdes opposants au président turc R.T. Erdoğan
  
Syrie 
========

Nous célébrons avec le peuple syrien la chute du régime de Bachar El Assad, 
ayant provoqué la mort de centaines de milliers de syriens, la destruction 
massive des infrastructures, emprisonné et torturé des dizaines de milliers 
de personnes. 

La Syrie de Bachar El Assad était devenue un narco-Etat, plaque tournante du 
trafic de captagon.

L’offensive militaire des factions rebelles, qui a joué un rôle décisif dans 
la chute du régime syrien, était accompagnée par une aspiration populaire 
immense pour en finir avec le régime corrompu, sanguinaire et mafieux de
Bachar El Assad. 

La chute du régime est le prolongement de la révolution populaire syrienne 
déclenchée en 2011, portant l’exigence de démocratie, de droits et libertés 
fondamentales.

L’avenir est très incertain pour la Syrie et la région : risques de fragmentation 
avec les interventions armées de la Turquie et d’Israël, interrogations sur 
ce que fera le nouveau pouvoir vis à vis des droits des femmes et des minorités.

Nous soutenons les revendications pour l’unité du pays et pour un État 
démocratique, laïque et civil, garantissant l’égalité des droits entre les 
femmes et les hommes.

**Le Rojava : un acquis démocratique précieux pour l’avenir de la Syrie**
============================================================================

Le Rojava, symbole de la résistance kurde, représente **une expérience démocratique 
unique au Moyen-Orient**.

Après avoir combattu et vaincu l’État Islamique en 2015, notamment à Kobané, 
symbole de cette lutte, les combattantes et combattants du Rojava ont mis en 
place sur le territoire libéré une société démocratique, multiethnique,
féministe, où les femmes et les hommes sont à parité à tous les niveaux 
(conseil municipal, administration, armée, …), où toutes les personnes vivent 
à égalité de droits, quelque que soit son appartenance culturelle, sa langue, 
sa religion.

L’expérience démocratique du Rojava (régions du Nord et de l’est de la Syrie) 
est sans pareille dans tout le Moyen Orient, née au coeur d’un pays ravagé 
par une dictature meurtrière.

Une agression meurtrière contre le Rojava sous l’égide du pouvoir turc
--------------------------------------------------------------------------

Depuis des décennies, le gouvernement de Recep Tayyip Erdogan mène une guerre 
implacable contre les Kurdes, à la fois à l’intérieur de ses frontières et 
dans les régions voisines. 

**Les milices de l’"Armée Nationale Syrienne", armées et appuyées par le pouvoir 
turc, notamment avec des drones , profitent du chaos pour attaquer le Rojava**. 

Des dizaines de milliers de personnes ont déjà été déplacées.

Nous appelons la France à 
----------------------------

- Exiger le retrait des forces d’occupation étrangère du pays, en particulier 
  la Turquie et Israël, pour permettre au peuple syrien la possibilité d’enfin 
  s’autodéterminer et aux minorités d'être protégées ;
- Soutenir les efforts de transition pacifique, basée sur la justice, le droit 
  et la prise en compte des aspirations de toutes les composantes du peuple 
  syrien, en respectant ses minorités et les catégories les moins protégées 
  de sa population ;
- garantir le droit aux Syrien·nes refugié.es en France et pouvoir rester 
  si iels le souhaitent et pouvoir se rendre temporairement en Syrie sans 
  perdre leur statut.
  
Avec le soutien de 
===================
  
- AFPS, 
- Algérie au coeur, 
- ATTAC 38, 
- CISEM, 
- LDH, 
- CNT 38, 
- Ligue de défense des droits de l'homme en Iran (LDDHI), 
- Les jeunesses internationalistes, 
- Mouvement de la Paix, 
- MRAP, 
- Nil Isère, 
- UJFP, 
- UD-CGT 38, 
- FSU 38, 
- Luttes de classes Éducation, 
- Solidaires, 
- Union Etudiante de Grenoble, 
- ADES, 
- Ensemble! Isère, 
- Génération·s Isère, 
- Go Citoyenneté, 
- La France Insoumise, 
- NPA anticapitaliste, 
- PCF, 
- PEPS 38, 
- UCL (Union des communistes libertaires)
