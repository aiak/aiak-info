.. index::
   pair: Berivan Firat ; Histoire de la résistance kurde (2025-01-26)

.. _berivan_firat_2025_01_26:

================================================================================================================================================
2025-01-26 Réunion publique **Histoire de la résistance kurde** avec Berivan Firat dimanche 26 janvier 2025 17h30 à Saint-Martin d'Hères
================================================================================================================================================


Berivan Firat
=================

- :ref:`kurdistan_luttes:berivan_firat`


Maison de quartier Gabriel Péri 16 rue Brossolette à Saint Martin d'Hères (tram C arrêt Péri Brossolette) à Saint Martin d'Hères
=====================================================================================================================================

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.752482712268829%2C45.188498448215576%2C5.756023228168488%2C45.19010333973849&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.189301/5.754253">Afficher une carte plus grande</a></small>
   

maison de quartier Gabriel Péri 16 rue Brossolette à Saint Martin d'Hères
(tram C arrêt Péri Brossolette) à Saint Martin d'Hères

Quelle est la situation des Kurdes au Moyen Orient ?

Réunion publique avec avec Berivan Firat **Histoire de la résistance kurde** 
================================================================================

.. figure:: images/affiche.webp


Porte parole du CDK-F (Conseil démocratique kurde en France) responsable des
relations internationales

En Syrie, après la chute du régime de Bachar El Assad, l'avenir est très
incertain pour la Syrie et la région : risques de fragmentation avec les
interventions armées de la Turquie et d’Israël, interrogations sur ce que
fera le nouveau pouvoir, capacité de la société civile syrienne à peser pour
éviteer tout retour à l'autoritarisme, à défendre les droits des femmes et
des minorités.

En Turquie, alors que le régime agresse militairement le Rojava, deux députés
du parti DEM ont pu rencontrer A. Ocalan et rendre publique une déclaration du
leader du PKK, tandis que Recep Tayyip Erdogan s'est publiquement félicité des
"progrès" réalisés dans le dialogue avec le PKK. 

**Comment interpréter une politique vis à vis des Kurdes qui semble aussi contradictoire ?**


En Iran, **le régime, très fragilisé depuis le mouvement "femmes, vie, liberté", réprime les militantes et militants en procédant à des exécutions massives**. 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------

- :ref:`iran_2025:perrin_2025_01_18`

En Iran, **le régime, très fragilisé depuis le mouvement "femmes, vie, liberté", 
réprime les militantes et militants en procédant à des exécutions massives**. 

Le nombre de femmes exécutées est en très forte hausse, la répression qui frappe
les femmes kurdes est massive et cruelle comme vient le rappeler la confirmation
de la condamnation à mort de la militante de la société civile :ref:`Pakhshan Azizi <iran_2025:pakhshan_azizi_2025_01_28>`

Dimanche 26 janvier à 17h30 à la maison de quartier Gabriel Péri (tram C
arrêt Péri Brossollette), nous organisons une rencontre avec Berivan Firat,
porte parole du CDK-F (Conseil démocratique kurde en France) et responsable des
relations internationales.

Elle fera le point sur la conjoncture actuelle et sur la résistance kurde.


Intervention principale de Berivan
=======================================

- https://journals.openedition.org/teth/3463 (Le Rojava : une alternative communaliste à l’État-nation)


.. note:: une erreur fait que les 3 premières minutes n'ont pas été enregistrées

.. youtube:: 8zMxoEaGwuI

...

Et il y a le silence. Personne n'en parle. Comme si c'était normal. 

L'HTS, l'armée nationale syrienne, les turcs, ils les chassent et les coupent 
comme des moutons. Personne n'en parle. 

On a taillé la barbe des loups, on en a fait des agneaux, on leur a mis des 
costumes. Et là, ils sont en train de pourchasser des êtres humains. 
Et on n'en parle pas. Alors, quand on parle des criminels, comme Assad, 
comme Saddam, comme les mollahs, mais qu'on ne parle pas des choses
qu'on voit, est-ce qu'on n'est pas plus responsable que nous ? 

Est-ce que nous avons la force de pouvoir dénoncer ? De pouvoir dire qu'il y a 
une injustice, de pouvoir dire qu'il y a des gens qui se font massacrer. 

Je ne le dis tout autant pas parce que je suis aussi en même temps Alévi. Parce qu'en tant
qu'être humain, je suis en fait Alévi de mes parents. 
Je suis moi-même assez éloignée. Vous m'avez compris. Mais simplement parce que c'est libre. On
ne peut pas tenir responsable une minorité de la barbarie d'un individu. 

C'est comme si demain, nous libérons, nous rendons des mollahs et nous allons aller
massacrer tous les chiites parce qu'on va dire, voilà, vu que vous étiez de
la même religion ou même conviction que les mollahs, vous êtes responsable
de ce qu'ils ont fait. 

Qu'est-ce que c'est que cette mentalité ? 

Qu'est-ce que c'est que cette politique qu'on essaie de nous vendre ? 

Alors, je vous parle de la Syrie. Parce que malheureusement, ça nous concerne directement,
nous les Kurdes. Je vous parle de l'Iran parce que malheureusement, ça nous
concerne directement. Je vous parle de l'Irak. Ça nous concerne. Je vous parle
de la Turquie. En tant que Kurdes, ça nous concerne. Et vous allez dire, mais
qu'est-ce que vous faites dans tous ces pays ? Nous n'y faisons rien. Nous étions
là. Ces pays sont venus s'installer sur nos territoires. Et malgré cela, nous
avons... Nous n'avons pas un projet politique à dire, attendez, on va retracer
les frontières du Kurdistan. Donc vous les Perses, dehors. Vous, les Arabes ou les
autres, dehors. Les Chaldéens, dehors. Les Arméniens, dehors. Ceux qui sont sur
nos terres. Et maintenant, on va vivre sur nos terres en tant que Kurdes. Non. Nous
avons un projet politique de vivre ensemble. Un projet politique multi-ethnique,
paritaire, féministe, écologique. Un projet politique qui a été déjà mis
en place dans la région du Rojava. Et en parlant de la région du Rojava,
je voudrais le répéter. Aujourd'hui, c'est la dixième date anniversaire
de la libération de Kobani. Kobani a résisté, Kobani a vaincu. Et Kobani
continue à résister. Tichine continue à résister parce que l'armée barbare
turque continue à bombarder avec les armes de l'OTAN. Les Kurdes. Ceux qui ont
débarrassé l'humanité des Daesh et tout le Bataclan. Excusez-moi, le Lapsus. De
toute compagnie. Ces mêmes barbares qui sont venus aussi attaquer justement au
Bataclan. Charlie Hebdo, Lille, Bruxelles, Strasbourg et ailleurs. Et aujourd'hui,
certains de leurs cadres sont soi-disant au gouvernement. Nouveau gouvernement,
Cyril. Et on demande aux Kurdes qui ont perdu. Ce n'est pas 12 000. Ça a été
dernièrement rectifié. Parce qu'on nous accusait, nous les Kurdes, d'aller
aider nos frères en Syrie. Ce n'est pas 12 000. C'est 25 000 Kurdes qui sont
morts dans le combat contre Daesh dans le nord et l'est de la Syrie. On a parlé
de 12 000. Parce qu'on n'a pas parlé des Kurdes de Turquie qui étaient venus
pour combattre contre Daesh. Parce qu'on disait, on va dire que c'est le PKK qui
est venu combattre. Oui, mais le PKK quand il est venu, le parti des travailleurs
du Kurdistan, l'armée française, la coalition était à côté d'eux. J'ai la
preuve, j'ai la photo de mon fils. La coalition internationale est à côté, il
est même en train de monter la garde. Et aujourd'hui, on demande aux Kurdes. Du
Lojava, donc du nord et l'est de la Syrie, ces Kurdes qui sont partis combattre
les obscurantistes à Membitch, à Raqqa, qui n'étaient pas des villes kurdes. Et
une fois ces villes libérées, ils se sont retirés. Ils les ont laissés à
des assemblées civiles. Les Kurdes ne sont pas allés occuper le territoire des
autres. Ils se sont retirés sur leur terre. Il y a des assemblées populaires. Les
gens, les habitants de ce lieu qui s'autodirigent. Alors on nous dit, laissez
les armes. Maintenant, il y a Saddam, pas Saddam, c'est pour les introvertis
aussi, on confond les noms. Assad a été renversé. Il y a un gouvernement
provisoire, sauf que c'était ceux-mêmes qui nous ont coupé les têtes, qui
ont vendu les femmes et les enfants et les petits garçons, et ils étaient sur
les marchés en tant qu'esclaves. Qui encore, près de 3000 femmes et petites
filles et petits garçons, et ils y sont toujours portés disparus. Ces mêmes
individus qui commencent à couper, qui continuent à couper la tête des Alevis,
des Duruz et des autres. Et tout ça dans le silence. De tous. Parce que ces
mêmes individus se sont alignés sur la politique de Serb. Sauf que ces groupes
ne sont pas homogènes. Il y a 18 groupuscules qui les composent. Ils disent,
il y a 18 groupuscules, on n'arrive pas à tous les contrôler. Donc quand ils
coupent la tête des Alevis, des chrétiens d'Orient, des Duruz et des autres, on
dit oui mais... HTS n'est pas homogène, il y a 18 groupuscules à l'intérieur,
on ne peut pas tous les contrôler. Vous avez réussi à les contrôler, à les
amener au pouvoir ? Les laisser arriver jusqu'au pouvoir ? Parce qu'ils ne sont
pas arrivés tout seuls ? Mais vous n'êtes pas capable de les contrôler pour
empêcher qu'il y ait un génocide sur une minorité culturelle qui depuis des
siècles est persécutée. Que ce soit en Turquie, que ce soit en Iran, que ce
soit en Irak ou ailleurs. Et tout ça... Nous sommes tous d'une façon complice
parce que nous sommes tous silencieux. Aujourd'hui, à la date anniversaire de
la libération de Kobané, il y a des bombardements sur le barrage de Tichelé,
le barrage de Tichelé qui est sur le point de lâcher, car c'est bombardé
depuis le 12 janvier. Depuis le 12 janvier, il y a eu 22 civils qui sont morts,
qui sont portés, qui sont partis monter la garde. 22 civils sont morts,
dont un acteur de théâtre kurde. C'est extrêmement important. Bab-e-Taher,
qui depuis des décennies faisait du théâtre en kurde, alors que la langue
kurde était interne. Elle était interne. Il y a des dizaines, des millions
peut-être, de gens ont appris le kurde grâce à ses sketchs, grâce à ses
petits... Comment je veux dire... pièces de théâtre. Il a été tué parce
qu'il a monté la garde pour pas que ce barrage ne cède. Si ce barrage cède,
les gens de la région vont se retrouver sans électricité, sans eau. Et il y
aura une catastrophe écologique. Et de plus, la route internationale sur laquelle
l'Europe et les États-Unis comptent, parce que ça va être la nouvelle route
de soi, va passer aux mains des Turcs. Et les plus barbares sont aux côtés de
qui ? La Turquie est le chef du terrorisme international. Que plus personne ne le
dise, non, c'est pas vrai. La Turquie est le chef du terrorisme international. Il
a aidé, amené jusqu'à là, que ce soit l'armée nationale syrienne, que ce
soit HTS, que ce soit Daesh, Al-Qaïda et les autres. Et aujourd'hui, on nous
dit, HTS disait, Jaloni qui disait aujourd'hui, on peut, avec la Turquie, lutter
contre Daesh. On va remettre des brebis à ces barbares, à ces loups pour que,
soi-disant, ils débarrassent les curés, les loups et les dangers. Et ce sont
eux, les dangers. Donc, quand la situation est ainsi, on essaie encore une fois,
au niveau de la Turquie, parce que les jeux ottomans en Turquie ne se terminent
pas, l'extrême droite, c'est pas Erdogan, c'est pas la gauche, c'est l'extrême
droite, celui qui disait, même si je bois le sang des Kurdes, je resterai toujours
assoiffé, a dit, tiens, Öcalan pourrait venir à l'Assemblée nationale, il va
s'adresser aux Kurdes. Et là, d'un coup, on a dit, ah mais, il y a de nouveau
des pour parler de paix, on parle de paix, donc Öcalan va venir, va sortir, et
il va de nouveau avoir des discussions pour une solution kurde. Sauf que Öcalan
est toujours en isolement carcéral, il est en prison, sur une rue. La Turquie
décide quand ? Il peut voir ses avocats. La Turquie décide quand il peut voir
sa famille. Pendant 46 mois, il n'a eu ni le droit de la visite de ses avocats,
ni de ses familles. 46 mois en isolement carcéral total sur une rue. Et on lui
envoie une délégation, enfin on lui envoie son neveu, qui est en même temps
député. Il le voit pendant une heure. On lui dit, il peut venir parler à
l'Assemblée, nationale, mais il va venir à l'Assemblée nationale. Après 50
ans de guerre, plus de 50 000 personnes qui ont perdu la vie, de Kurdes qui ont
perdu la vie au combat, Öcalan va dire bon, maintenant, on va déposer les armes
et on va se rendre aux Turcs. Il n'y a pas d'enfants, les petits mineurs, je leur
dis ça, ils vont révolter. Donc, il y a pas de pour parler de paix. Il n'y a
pas de débat, de discussion de paix. Il y a quelque chose. Mais donc, on ne peut
pas expliquer ce que c'est. La Turquie est en grande difficulté. C'est vrai que
d'un côté, il a amené ses vermines au pouvoir en Syrie. Mais de l'autre côté,
la première chose qu'ils ont fait, ils ont instauré un droit de droit de 500
% pour les produits turcs. Donc, ils sont avec les Turcs, mais en même temps,
ils ne sont pas avec les Turcs. Et de l'autre côté, les États-Unis et Israël
ont passé un message à la Turquie en disant attention, tu vas trop loin. Si
tu vas trop loin, tu vas te retrouver coincé dans l'Anatolie centrale. C'est
à dire que tous les territoires kurdes et notamment aussi, comme on appelle
ça, l'endroit où il y a eu les tremblements de terre qui appartiennent à
la Syrie. Si tu vas trop loin, tout ça, tu vas le perdre. Tu vas te retrouver
coincé dans l'Anatolie centrale, qui est ton territoire. Donc, ils se retrouvent
à un moment donné à dire qui c'est qui va me sortir de cette situation ? Ce
sont les Kurdes que je massacre depuis des décennies. Sauf que les Kurdes,
que ce soit en Turquie, en Iran, en Syrie ou en Irak, sont le peuple le plus
politisé de la région. Vous savez pourquoi ? À force qu'ils nous fassent du
mal, à force qu'ils nous massacrent, à force qu'ils nous tapent sur la tête,
on relève la tête. On essaie d'apprendre, on essaie de comprendre. Et plus on
comprend, plus on a envie de comprendre. Plus on a envie de comprendre, plus on
lit. Plus on lit, plus on a envie de lire. Plus on a envie de lire, plus on a
envie de partager. En nous torturant, en nous maltraitant, ils ont fait de nos
femmes de 70, 80 ans illettrées des philosophes de la société. Des gens qui
sont capables de prendre un micro et de vous tenir un débat sur la situation de
la société, sur la lutte des femmes, sur l'avenir du Moyen-Orient, sur ce qui
est bien, ce qui est mal pour nos sociétés, comment les femmes peuvent être
vives, comment nous devons élever des enfants. Ces femmes à qui on a interdit
de parler leur langue, qui ont élevé des enfants, qui ne parlent pas un mot
du kurde alors qu'elles, elles ne parlent pas un mot du kurde. Ces femmes qui
ont été demandées prison, pour voir leur enfant incarcéré pour des délits
politiques et qui ne pouvaient pas leur parler parce que s'ils parlaient en kurde,
non seulement le fils ou la fille étaient torturés, mais en plus, elles, elles
étaient torturées, elles ne pouvaient plus jamais aller voir leurs enfants. Ces
mères ont réussi à élever des enfants qui ne parlaient plus du kurde. Mais
avec la lutte menée par les Kurdes, par le mouvement de libération kurde, par la
direction du parti des travailleurs du Kurdistan, avec le projet politique mis en
place, développé par Öcalan et mis en place notamment au Rojava et ailleurs. Eh
bien, ces femmes, ces hommes qu'on a condamnés au silence, ces hommes et ces
femmes qu'on a considérés comme des sous-personnes, comme des personnes de
troisième zone, sont devenus des interlocuteurs. Il y avait un journaliste,
extrêmement connu, turc, qui d'ailleurs, sur le départ, avant de mourir, a
découvert qu'il était kurde en réalité. En réalité, il s'appelait Mehmet
Ali Biran. C'est le plus connu de eux. Je crois plutôt qu'il n'avait pas osé
dire qu'il était kurde. C'est sûr, avant de mourir, il a dit qu'il était
kurde. C'est lui qui disait, c'est bizarre, la première fois qu'il a été
à Diyarbakir Ahmed, qui est le capital du territoire national joannatricement
kurde, il a dit j'ai vu des femmes illettrées, mais dès qu'elles prennent le
micro, elles deviennent des femmes qui débattent, qui parlent, qui sont en train
d'analyser la société. Et à l'époque, il ne savait pas qu'il était kurde. Il
a dit en réprimant les Kurdes, on a fait de la société kurde, la société
kurde, la société la plus politique du Moyen-Orient. Et c'est aujourd'hui une
société comme ça qu'on essaie, encore une fois, de tromper. En Turquie, on
essaie en soi-disant de parler de paix, mais au final, on apprend qu'on demande,
venez à l'Assemblée nationale et dites, on dépose les armes. Ça va pas ou quoi
? Pourquoi je dois déposer les armes au moment où tu es le plus coincé ? Viens,
on débat comment on résout la question kurde. Comment on résout le fait que
les Kurdes ne sont pas reconnus en tant que citoyens par la constitution kurde
? Le fait que les Kurdes n'ont pas le droit de parler légalement leur langue,
qu'elles n'ont pas le droit d'apprendre leur langue dans les écoles. Comment
ça se fait que pendant des décennies, les Kurdes sur leur propre terre en
Syrie étaient des apatrides ? Comment ça se fait qu'en Iran, les Kurdes ne
peuvent pas vivre en tant que citoyens comme tous les autres ? Bon, maintenant,
toute la société est réprimée, ça c'est une autre chose, mais je vous parle
de... Comment ça se fait qu'en Irak, 182 000 personnes ont été massacrées du
mois de mai 87 au 16 mars 88, que les armes chimiques ont été essayées sur les
Kurdes, et qu'on a dit, les Kurdes sont des citoyens pareils que les autres. Alors
c'est quand on meurt, c'est quand on se tait, qu'on devient des bons citoyens,
eh bien les Kurdes, ils ont décidé de parler. Les Kurdes ont décidé de
mener une lutte, ont décidé de continuer leur lutte. Et en plus, avec, sous
la direction ou sous la gestion des femmes, ça les rend malades. Les barbares,
les réactionnaires, les démocrates, et tout ça les rend malades de voir des
femmes qui parlent, des femmes qui luttent, des femmes qui décident. Car les
femmes sont des forces, la force qui est capable de changer la société. Si
la femme change sa vision de la vie, elle change sa façon d'élever son
enfant. Elle change sa façon de regarder la société. Elle change sa façon
de regarder. Autour d'elle, elle change l'avenir de la société. C'est pour
ça qu'on a peur des femmes. On a peur que la femme puisse évoluer mentalement
et qu'elle puisse au final demander à ce que la société soit plus paritaire,
soit égalitaire. Pourquoi on parle par exemple de la révolution jengihanazadi,
femme, vie, tu le dis comment ? Liberté. Non, non, ça, je le sais, mais je
voulais le dire en persi. Zan Zendidi Hazadi. Zan Zendidi Hazadi. Parce que je
veux rendre hommage à mes soeurs iraniennes. Pourquoi on parle simplement de
dire les femmes iraniennes, regardez les, regardez les, les bras avec son fort,
qu'elles enlèvent leur foulard. Comme si leur problème, c'était l'histoire
d'enlever le foulard. C'est pas ça l'histoire. On a réduit la révolution,
le soulèvement de ces femmes, oui, de ces braves femmes, de vouloir changer la
société, de vouloir mettre fin à la dictature des théocrates, des mollahs. Et
on est en train de le dire en Europe, comme si l'histoire, la demande des femmes
iraniennes, c'était d'enlever le foulard. C'est pas ça l'histoire. Pourquoi
vous voulez réduire la lutte d'émancipation, la lutte pour le droit, la lutte
de pouvoir vivre ensemble, la lutte de pouvoir changer la société et donner
un avenir différent à nos enfants, comme une histoire de foulard. Mais zut
alors, combien de femmes iraniennes ou Kurdes de Rojala, parce que c'est comme
ça qu'on les appelle nos Kurdes d'Iran, se sont levées en dire mais attendez,
stop, mais c'est pas une histoire de foulard. Sous le foulard, on a une pensée,
une idéologie, qui est capable de changer le monde, qui est capable de vous
faire rentrer dans l'armée, de faire rentrer l'armée dans les casernes et de
pouvoir avoir un avenir meilleur pour nous, les femmes et surtout pour l'avenir
de nos enfants. Pourquoi l'intelligentsia d'Iran, les Kurdes de Rojala, d'Iran
ou de Syrie sont obligés de partir ailleurs ? Pourquoi ils ne pourraient pas
vivre sur leur terre ? Parce qu'il n'y a pas de liberté. Parce qu'il n'y a pas
la possibilité de pouvoir vivre, pouvoir vivre simplement sans être obligé de
rendre des comptes à des mollas qui se mêlent de comment je m'habille, qui se
mêlent de comment je mets mon foulard, qui me disent que je ne dois pas penser
comme ça, qui me disent que je ne dois pas agir comme ça, qui me disent que je
ne dois pas être avec lui, qui me disent que je ne dois pas être... Qui vous
êtes pour décider ? Et qui vont condamner des gens pour soi-disant révolte
contre Dieu ou je ne sais pas quoi. Mais qui vous êtes ? Ce Dieu qui est
tellement immense, il n'est pas capable de nous punir et qu'il est tombé avec
des barbus contre vous. Vous n'êtes même pas capable de vous laver les dents
et vous êtes en train de condamner en nom de Dieu ? Quel affront vous faites
au Dieu ? Auquel vous croyez ? On ne dit pas, il est au dessus de tout. Et il a
besoin des petits, de sales gosses comme vous, des dégueulasses. Comme vous pour,
je ne sais pas quoi, pour faire le ménage sur terre. On dit, wow. C'est lui qui
a créé l'univers, nous ? C'est lui qui a créé les religions ? C'est ce que
vous dites, moi je ne suis pas courante, je suis désolée. Respect à tous. Je
reprends ce qu'il dit. C'est lui qui a créé femme, homme, noir, blanc, rouge. Il
a besoin de toi pour faire la loi. S'il trouve que le jeune homme n'est pas bien,
tu ressembles à mon fils, donc je vais te faire te fâcher, je vais te prendre
en étape. En plus elle tombe plus rouge. Allez-lui me dérange. Il fait des
choses qui ne sont pas, je ne sais pas trop quoi. Il n'est pas croyant. Et Dieu,
Allah ou peu importe qui est là-haut ou qui est ailleurs, comme les allées qui
disent qu'il est dans leur cœur, ils disent que Dieu est partout. D'accord. Il
n'est pas capable de faire le nécessaire. Ils disent que les chrétiens, les
barbus là, ils disent que les chrétiens sont mauvais. Pourquoi ? Ce n'est
pas Dieu qui les a créés ici. Mais il n'est pas capable, s'il les a créés,
de s'en débarrasser. Il a besoin de toi. Non, mais pour qui vous nous prenez
? Vous n'êtes même pas capable de défendre la religion. Comment je veux dire
? D'expliquer la religion auquel vous croyez. Moi, je ne suis pas capable de
gérer ma maison. Je ne suis pas capable de... Je n'ai pas en plus. Et je vais
me mêler de l'histoire, de l'humanité, de vouloir faire la loi au nom d'Allah,
au nom de Dieu, au nom de... Et il va avoir, d'autres qui vont croire à ça. On
va battre, on va humilier, on va assassiner des femmes et des hommes parce qu'ils
représentent quelque chose qui est différent de nous. On va se mettre à la place
du puissant. Dans toutes les religions, c'est ce qu'on dit. Et on va... Comment
je vais dire ? Un coup de pierre, un coup de... On va assassiner les femmes,
on va assassiner LGBT, on va assassiner les gays, qui vous êtes, bon sang ? Qui
vous êtes ? Donc, c'est dans ce... Dans tout ce dynamisme de certains individus,
de certaines groupuscules qui se mettent au nom du soi-disant créateur, que
nous, en tant que femmes kurdes en premier, nous avons dit, nous enfantons, nous
donnons la vie. Avec la contribution des hommes, bien sûr. Pas beaucoup. Oui,
quand on regarde la douleur, tout ça de l'accouchement et tout ça, c'est vrai
que la contribution est minime. Nous, nos enfants, quand nous sommes enceintes,
quand nous accouchons, ceux qui ont fait un petit peu d'études de recherche le
savent, quand un enfant est né, il laisse une partie de son ADN dans le corps
de sa mère. C'est logique. C'est-à-dire que je porte l'ADN de mon fils, de
mes fils. Donc, les enfants qui sont toujours en train de grandir en moi, qui
sont la prunelle de mes yeux, vont être assassinés pour... Comment je veux
dire ? Pour des futilités. Et moi, en tant que femme, je ne vais pas vouloir
me lever, parce qu'en tant que mère, moi-même victime de guerre, je ne vais
pas me lever pour dire, moi, je souffre, mais je ne veux pas que elle, qui est
une femme comme moi, qui est une mère comme moi, elle souffre la même chose
que moi. Parce que c'est tellement violent, c'est tellement inexplicable, c'est
tellement douloureux de perdre un enfant, quand on dit, on ne le souhaite pas
à son ennemi, c'est vrai. C'est pour ça que j'ai décidé de continuer. J'ai
décidé d'être la voix de mes sœurs, pas seulement de mes sœurs, parce que
nous, notre projet politique, c'est de réussir à vivre ensemble. Ou ici, on
n'y arrive pas, en Europe. Les musulmans, les chrétiens, les islamistes, les
antisémites, les sionistes, qu'est-ce que je pourrais dire encore pour mettre
au monde ? Il y a toujours quelque chose qui fait qu'on divise nos sociétés,
pour mieux régler en réalité. Et quand il y a un projet politique qui,
malgré la guerre, s'apprête, notamment le projet politique du Rojava,
de Kobani, où les sociétés vivent sous une démocratie directe, qui est
le confédéralisme démocratique, où il y a des assemblées populaires qui
s'autogèrent, et il y a des, comment on pourrait dire, comme des crans, qui,
si on n'arrive pas à régler le problème, on va... C'est-à-dire que c'est le
triangle à l'envers. Ici, c'est un gugusse qui décide pour nous, et hop, la
décision elle arrive en bas, et on est obligé de s'exécuter. Alors que là,
nous prenons les décisions au niveau populaire. Si il y a des commissions, des
assemblées de quartier... Par exemple, j'avais été dans un village, au Rojava,
il y avait ce qu'on appelle une réunion populaire, justement, et ils avaient
appelé tous les habitants du village, ils avaient mis des chaises, il faisait
tellement chaud qu'ils avaient attendu sur le soir, il y avait même le soleil,
on le voyait, mais on voyait pas le... Comment je veux dire, comme si c'était
la nuit, quoi. Et ils avaient appelé tout le monde, et à un moment donné,
ils ont dit, tout le monde est là, ils ont regardé, et il y a une vieille dame
qui se lève et elle dit, non, non, non, je m'en rappelle plus le nom, c'est un
arménien, mais je vais vous dire un mot, parce que c'est le prénom que je connais
le plus d'Arméniens, elle dit, non, non, tonton, un peu, il est pas là. Ah ben,
il a dit, non, il faut aller le chercher. Donc, il y a un jeune qui est parti,
et on a attendu peut-être une dizaine de minutes, on a vu un papy qui arrive,
qui a du mal à marcher, en plus. Mais le village, il est pas là. Il y avait
peut-être autant de monde que ça. Et ils ont dit, on ne peut pas commencer,
on ne peut pas faire notre assemblée du village, si... un des couleurs de
ce village n'est pas représenté. Cette couleur, c'était l'arménien, le
seul. Toute la famille était en Europe. Lui, il n'avait pas voulu quitter son
village, sa maison. Il était âgé, il voulait vivre et mourir sur cette terre
où il était né. Et ils ont dû attendre qu'il arrive, pour qu'il puisse choisir
l'assemblée populaire, et que lui, il en fasse partie, parce que s'il a besoin
de quelque chose, il va le faire. Et il a besoin de quelqu'un qui soit musulman,
ou... l'autre chrétien ne va pas le comprendre. Il est chrétien orthodoxe, donc
il faut que quelqu'un... il se représente. Et c'est dans un projet politique
comme ça, qu'on veut détruire, qu'on veut, à un moment où on est encore en
train de... de vouloir nous couper la tête, on demande aux Kurdes de déposer
les armes, qu'on demande aux Kurdes de... qui sont venus, qui ont traversé
les frontières, en se tapant dans les parpelets pour aller aider... pour aller
aider leurs frères et sœurs qui étaient dans la lutte face à Daech et face à
Assad aussi, parce qu'on n'en parle pas suffisamment. Ils pensent que les Kurdes
étaient aux côtés d'Assad, ce n'était pas du tout le cas. Ils sont allés
combattre là-bas. Ils sont tombés et ils sont restés pour qu'ils puissent
se protéger jusqu'à ce qu'il y ait une stabilité. Aujourd'hui, on dit, les
Kurdes de Syrie, vous pouvez rester là, vous déposez les armes, et vous allez
vous fondre dans la nouvelle catégorie de... de barbares soft. Mais ceux qui
ne sont pas de nationalité syrienne, vous repartez chez vous. C'est-à-dire,
vous partez du côté, soit Rojava, iranien, il y en a, beaucoup, ou du côté
turc. Par contre, c'est ce que je disais, les Turcmen, les Turcs, les Ouïghours,
et tous les autres qui étaient là pour, soi-disant, faire le djihad, décapiter
les gens, pour violer les Hizdis, qui étaient pour eux de je ne sais pas
quoi, en une nuit, on les a naturalisés. On en a fait des Syriens pour qu'ils
puissent prendre leur place au sein du nouveau gouvernement. Mais aux Kurdes,
on demande de quitter leur terre. C'est incandescent qu'ils soient du côté
syrien, iranien, irakien. Aux Turcs, c'est notre terre. Moi, j'ai... ma maman,
elle avait des... elle était la seule, elle avait des frères, des oncles,
des tantes, et tout ça, qui étaient restés du côté de Rossa. Et là, elle
s'était retrouvée... elle avait de la famille du côté iranien, manjarat,
et elle, elle était devenue kurde. En une nuit. Parce que, ben, ils avaient
divisé le territoire. Et ils étaient un peu pas loin. On a pris une équerre,
on a tracé. Oups. Turquie, Iran, Irak. Donc voilà. Et l'intégrité territoire,
territoriale, l'intégrité physique des Kurdes, on n'en avait rien à faire. On
continuait à faire la même chose. Sauf que les Kurdes, avec tous les sacrifices
que nous avons faits, avec toute la détermination que nous avons dans ce projet
politique de vivre ensemble, on ne peut pas... on ne peut pas reculer. Peut-être
que les hommes, un jour, ils diront nous sommes fatigués, nous déposons les
armes. Nous, les femmes, nous continuerons le combat. Il est hors de question que
nos enfants soient morts pour rien. Tant qu'il n'y aura pas la liberté pour les
Kurdes, il est hors de question que la lutte, où que nous soyons, ne cesse. Et
en menant la lutte pour nos droits, en tant que Kurdes, nous n'oublions pas non
plus d'être activement solidaires avec les autres peuples russes. Je le répète,
les Tammouds, c'est un peuple qui nous fait énormément mal au cœur. Parce qu'ils
sont 90 millions, ils sont les plus parts amputés parce qu'ils ont été tués à
coups de machettes, qu'ils sont meurtris, des gens qui sont encore traumatisés,
car le jeunesse-tout a été tellement violent. Non seulement on les a massacrés,
mais en plus, on les a poussés dans le désert, au final jusqu'à la mer, où
plus de 150 000 ont disparu parce qu'on les a poussés dans l'eau. C'était soit
un coup de hache, soit ils essayaient quelque chose, mais la mer était tellement
loin des plages qu'il était impossible de s'en sortir. 150 000 sont... 180 000
sont disparus. Et ces gens-là ont même peur de se rassembler ici parce qu'ils
sont suivis et les informations sont transmises là-bas. S'ils ont des proches
et tout ça, ils sont morts. Donc la solidarité avec le peuple kurde, surtout
pour décriminaliser le mouvement kurde, car tant que le mouvement kurde n'est
pas décriminalisé, toute activité, même démocratique, peut jour et lendemain
être utilisée contre nous, ce qui est fait malheureusement. Il y a des retraits
de statut de réfugiés, il y a des gels d'avoir, quand on dit avoir, les pauvres,
ils ont généralement un salaire. Et donc il y a une pression constante sur les
Kurdes, car quand on combattait Rojava là-bas, pour aussi l'avenir de l'Occident,
nous étions les meilleurs combattants, les héros des temps modernes, mais une
fois que nous arrivons ici, nous sommes des criminels, des terroristes, ou si
nous combattons pour mourir pour l'Occident, pas pour mourir pour l'Occident,
mais pour nos droits, nous sommes tout de même des criminels, mais si nous
mourons pour eux, nous sommes des braves combattants. Donc cette duplicité doit
prendre fin, comme elle a pris fin pour Nelson Mandela, qui était un criminel,
qui était un assassin, qui était un terroriste, il avait été incarcéré
pendant 27 ans, il a été libéré, il est devenu président de l'Afrique du Sud,
et le monde entier s'est posté en devant lui, lors de son internat. Comme Yasser
Arafat, qui était aussi terroriste, qui est devenu président de l'autorité
palestinienne, comme Massoud Barzani, qui était un terroriste jusqu'en 2018,
je parle, et qui est devenu, après, il devait y aller aux Etats-Unis, donc on en
est de la liste. Et donc, aujourd'hui, la chose qui nous dérange le plus, c'est
cette criminalisation du mouvement kurde, et cette peur qu'on essaye de mettre
sur la tête des Kurdes, et il y a des Kurdes qui ont déchiré le lance-œil,
donc la peur, ça veut dire que nous Hijra, et le même exemple,

Réponse de Berivan aux remarques d'une femme Iranienne
===========================================================

.. youtube::  uK6uEvmZqWc


Tribunal Permanent des Peuples "Le Rojava contre la Turquie" les 5 et 6 février 2025 à Bruxelles 
========================================================================================================

- https://fr.wikipedia.org/wiki/Tribunal_permanent_des_peuples
- https://fondationdaniellemitterrand.org/agenda/session-du-tribunal-permanent-des-peuples-le-rojava-contre-la-turquie/

.. youtube:: YyhoJ9k9IZ0


Depuis le début de la guerre en Syrie, le Rojava (nord-est de la Syrie) s’est 
distingué par son projet de société inclusive, démocratique et égalitaire, 
qui a su rassembler les communautés kurdes, arabes, yézidies, chrétiennes 
et d’autres minorités dans un esprit de cohabitation pacifique. 

Ce modèle novateur repose sur les principes d’autonomie, de droits des femmes, 
de diversité culturelle et de justice sociale.


Cependant, depuis 2018, cette région est confronté à des attaques incessantes 
menées par l’État turc et des groupes armés qui lui sont affiliés. 

Ces opérations militaires, accompagnées d’exactions, ont profondément bouleversé 
les équilibres sociaux, politiques et humanitaires du Rojava. 

Il en résulte de graves violations des droits humains et de manquements au 
droit international humanitaire.


Articles
===========

- https://travailleur-alpin.fr/2025/01/28/saint-martin-dheres-debat-avec-berivan-firat-porte-parole-kurde-du-cdk-f/

Liens
===========

- https://kurdistan-au-feminin.fr/2025/01/27/danielle-simmonet-reclame-larret-de-lingerence-turque-en-syrie/
- https://fondationdaniellemitterrand.org/agenda/session-du-tribunal-permanent-des-peuples-le-rojava-contre-la-turquie/

