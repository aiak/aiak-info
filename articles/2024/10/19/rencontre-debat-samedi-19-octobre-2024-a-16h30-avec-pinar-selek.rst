

.. _pinar_selek_2024_10_19:

====================================================================================================================================================================================================
2024-10-19 |aiak| **Rencontre débat samedi 19 octobre 2024 à 16h30 avec  Pinar Selek sur son livre "Dans le Chaudron Militaire turc : un exemple de production de la violence masculine"** |pinar|
====================================================================================================================================================================================================


- https://travailleur-alpin.fr/2024/10/11/pinar-selek-sociologue-et-militante-persecutee-par-la-justice-turque-depuis-26-ans-invitee-a-saint-martin-dheres/
- https://pinarselek.fr/
- https://fr.wikipedia.org/wiki/P%C4%B1nar_Selek
- https://www.desfemmes.fr/auteur/pinar-selek/
- https://www.librairie-des-femmes.fr/livre/9782721012142-le-chaudron-militaire-turc-un-exemple-de-production-de-la-violence-masculine-pinar-selek/
- https://www.desfemmes.fr/essai/le-chaudron-militaire-turc/
- https://www.librairiesindependantes.com/

.. figure:: images/annonce.webp

.. tags:: PinarSelek, Turquie, Armée, Violence, Masculinisme, Patriarcat, Isere,
   Grenoble, SaintMartinDheres, SMH, AIAK, LibrairieDesFemmes, Antimilitarisme, 
   EcoFeminisme,



La vidéo avec Pinar Selek
===========================

- https://www.youtube.com/watch?v=X-UNBKUcAEE

.. youtube:: X-UNBKUcAEE


Un résumé des procès contre Pinar Selek par Chantal Morel (AIAK)
======================================================================

- https://travailleur-alpin.fr/wp-content/uploads/2024/10/Pinar-Selek-historique-des-proces.pdf

- https://www.youtube.com/watch?v=ORiRi8bZO1Q

.. youtube:: ORiRi8bZO1Q

Lieu : Maison des habitants, Romain Rolland, 5 avenue Romain Rolland 38400 Saint Martin d'Hères
====================================================================================================

- https://www.saintmartindheres.fr/smh_sitep/maison-de-quartier-romain-rolland/

::

    A la maison des habitants, Romain Rolland
    5 avenue Romain Rolland 38400 Saint Martin d'Hères  
    Accès : Arrêts bus C7 et bus 12 : La Croix du Pâtre

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.761980414390565%2C45.166734375173704%2C5.765520930290223%2C45.168339880239515&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.167537/5.763751">Afficher une carte plus grande</a></small>
   
Présentation
=================

- https://www.desfemmes.fr/pinar-selek-rencontres-dans-toute-la-france/
- https://travailleur-alpin.fr/2024/10/11/pinar-selek-sociologue-et-militante-persecutee-par-la-justice-turque-depuis-26-ans-invitee-a-saint-martin-dheres/
- https://travailleur-alpin.fr/author/maryvonne-matheoud/

.. figure:: images/persiste_et_signe.webp

   https://www.desfemmes.fr/pinar-selek-rencontres-dans-toute-la-france/

Rencontre débat samedi 19 octobre 2024 à 16h30 avec Pinar Selek |pinar|
sociologue, militante antimilitariste et éco-féministe turque
organisée par `Maryvonne Mathéoud, autrice <https://travailleur-alpin.fr/author/maryvonne-matheoud/>`_

Les causes économiques et politiques des guerres ne suffisent pas à 
comprendre ce qui les rend possibles ni comment elles parviennent à 
mobiliser les populations. 

Étudier le militarisme comme un processus social permet d’expliquer la 
naturalisation de la guerre ainsi que les mécanismes complexes de la 
structuration sociale et politique de la violence. 

Dans le Chaudron Militaire turc, j’ai analysé comment les pouvoirs 
politiques s’appuient sur les rapports sociaux de domination déjà existants 
et participent à leur reproduction, sur l’exemple du rôle de la masculinité 
normative dans l’organisation de la violence politique. 

Pinar Selek
Editions Des femmes Antoinette Fouque
EAN 9782721012142, 05/10/2023, 80 pages 


À propos du livre Dans le Chaudron Militaire turc : un exemple de production de la violence masculine
========================================================================================================

- https://www.desfemmes.fr/auteur/pinar-selek/
- https://www.desfemmes.fr/essai/le-chaudron-militaire-turc/
- https://www.desfemmes.fr/pinar-selek-rencontres-dans-toute-la-france/
- https://www.librairiesindependantes.com/


.. figure:: images/le_chaudron.webp
   :width: 500

Pinar Selek s'intéresse aux différentes étapes de la construction de la 
domination hégémonique masculine, essayant de "sonder les ténèbres qui 
font d'un bébé un assassin". 

L'enquête de terrain initiale menée en 2007, l'avait conduite à rencontrer 
des hommes d'origines géographiques et de milieux sociaux très différents 
sur plusieurs générations ayant seulement en commun d'avoir été obligés 
de faire leur service militaire, obligatoire en Turquie. Ce fut l'objet 
d'un premier ouvrage, "Service militaire en Turquie et construction de 
la classe de sexe dominante - Devenir homme en rampant" (L'Harmattan, 2014). 

L'autrice y étudiait les différents mécanismes à l'oeuvre pour formater 
les individus : dépersonnalisation, violence, soumission, absurdité et 
arbitraire d'ordres auxquels les jeunes appelés ne peuvent se soustraire, 
nationalisme et culte du pouvoir, de la force. 

Avec ce nouveau livre qui réarticule les éléments de ses recherches précédentes, 
Pinar Selek élargit sa réflexion, nourrie de références philosophiques à 
nos sociétés toutes entières, régies par un capitalisme effréné et un 
mépris à l'égard des femmes dans un contexte mondial de guerres et une 
montée des régimes autocratiques. Un texte fort et nécessaire. 

C'est dans un cadre très particulier, celui du système répressif turc, 
que Pinar Selek a mené son enquête, défiant la censure omniprésente. 

Exilée en France depuis 2011, elle est victime d'un acharnement judiciaire 
de la part de l'État turc depuis 25 ans et menacée de mort.

Annonces sur les réseaux sociaux
======================================

Travailleur Alpin
-------------------------

- https://travailleur-alpin.fr/2024/10/11/pinar-selek-sociologue-et-militante-persecutee-par-la-justice-turque-depuis-26-ans-invitee-a-saint-martin-dheres/
- https://x.com/Journal_LeTA/status/1844779405274456575

Mastodon
---------------

- https://kolektiva.social/@grenobleluttes/113332532718772556
- https://kolektiva.social/@noamsw/113298844199331369

Librairie la Nouvelle dérive
=================================

- https://www.lanouvellederive.com/edito

Le 12 avril 2021, La Dérive est devenue La nouvelle Dérive. 

Quelle histoire pour un petit adjectif ajouté! Un petit adjectif qui, 
avec ses deux “l” , ou deux ailes, a envie d’apporter un nouvel élan à 
la Dérive, sans rien lui enlever de son passé. 

Ici, en effet, dans cette petite librairie indépendante de quartier, on 
va continuer à vous laisser dériver vers des terres connues ou inconnues, 
en toute liberté. La porte est toujours ouverte (sauf en cas de grand froid 
ou de pluie battante), et elle invite à faire le détour, se laisser tenter, 
pour partir  en balade ou en expédition.




Compte rendu : **Saint-Martin-d’Hères. Pinar Selek raconte l’acharnement judiciaire turc** par Edouard Schone
===================================================================================================================

- https://travailleur-alpin.fr/author/edouard-schoene/
- https://travailleur-alpin.fr/2024/10/21/saint-martin-dheres-pinar-selek-raconte-lacharnement-judiciaire-turc/


Une conférence universitaire ciblée par le pouvoir turc
------------------------------------------------------------

Chantal Morel (Aiak) a ensuite détaillé, avec talent, l’historique de 
l’effroyable persécution judiciaire visant Pinar Selek, qui sera sans doute 
publié sur le site de son comité de soutien. 

"Il serait possible de résumer les faits du procès intenté à Pinar Selek 
de cette façon : la procédure dure depuis 26 ans, elle est accusée de 
terrorisme et a été déjà acquittée quatre fois, le fond du dossier est 
vide de preuves", a-t-elle souligné d’entrée

Cependant, "cela ne permettrait pas de prendre la mesure de l’acharnement 
contre Pinar Selek. La chronologie de ce procès, sans doute un peu 
fastidieuse, montre clairement l’ampleur de cet acharnement qui n’a 
plus que peu à voir avec la justice." 

Un texte accessible ici : `Pinar Selek- historique des procès https://travailleur-alpin.fr/wp-content/uploads/2024/10/Pinar-Selek-historique-des-proces.pdf <https://travailleur-alpin.fr/wp-content/uploads/2024/10/Pinar-Selek-historique-des-proces.pdf>`_

Chantal Morel a conclu son intervention en relatant les derniers éléments 
de l’affaire. 
Vendredi 28 juin 2024, s’est en effet tenu à Istanbul une nouvelle étape 
du procès de Pinar Selek. À cette occasion, le ministère de l’Intérieur turc, 
a produit une nouvelle pièce au dossier concernant une conférence universitaire.

"Dans le cadre du festival “Printemps des migrations”, soutenu par 
l’Université Côte d’Azur, l’Université Paris Cité, le CNRS et l’IRD et 
en lien avec ses travaux scientifiques, Pinar Selek a modéré une table ronde 
à laquelle participaient des femmes kurdes. 
C’est cet événement, considéré par le ministère de l’Intérieur turc comme 
un acte terroriste organisé par le PKK, qui a donné lieu à un nouveau 
chef d’inculpation devant le tribunal. 
Il s’agissait d’étayer la demande du tribunal à Interpol, afin d’établir 
des liens supposés entre Pinar Selek et le terrorisme", a indiqué la militante d’Aiak.

Et celle-ci de poursuivre : "Les institutions académiques françaises ont 
immédiatement protesté avec véhémence dans un courrier officiel adressé à
la cour. 
La mobilisation a payé : la manœuvre d’Ankara a pu être déjoué. 

Mais l’audience a confirmé l’acharnement judiciaire des autorités turques 
qui veulent obtenir l’extradition même si elles doivent bafouer les 
principes mêmes de la liberté académique."

Le juge a donc annoncé un nouveau report d’audience au 7 février 2025, 
"demandant la présence de Pinar Selek", tout en "refusant qu’elle soit 
entendue par voie de commission rogatoire, comme le permet pourtant la 
pratique de l’entraide pénale internationale", a déploré Chantal Morel.



La "banalité du mal" à l’œuvre en Turquie
-------------------------------------------------------

Présentant son livre Le chaudron militaire turc, Pinar Selek a quant à 
elle rappelé les thèses de la philosophe allemande juive Hannah Arendt 
sur la "banalité du mal". 
De fait, son ouvrage pointe les mécanismes qui se développent en Turquie 
et plus globalement en Europe sur les "sociétés de discipline" – 
pour reprendre le concept de Foucault.

Pour Pinar Selek, "on est passé à une société de contrôle. Hanna Arendt 
parle de trois éléments constitutifs de la banalité du mal en étudiant 
le nazi Eichmann, devenu un monstre : conformisme, indifférence à autrui, 
absence de pensée", a-t-elle précisé. 

"Mon apprentissage féministe m’a amené à compléter par **le désir de pouvoir**. 
Dans mes recherches sur les militaires en Turquie (cinquante personnes 
interviewées durant plusieurs jours), il apparaît que la masculinité 
joue un rôle majeur."

La sociologue a raconté et disséqué ce chemin de construction de la 
masculinité, qui joue selon elle un rôle majeur dans la société. 

Ceci au service du pouvoir autoritaire et des mafias qui prennent 
petit à petit une place prépondérante dans toute la société turque. 

La rencontre s’est terminée par un riche échange avec la salle.

