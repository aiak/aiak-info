

.. _pinar_selek_2024_10_19_14h:

====================================================================================================================================================================================================
2024-10-19 |aiak| **Rencontre débat samedi 19 octobre 2024 à 16h30 avec  Pinar Selek sur son livre "Dans le Chaudron Militaire turc : un exemple de production de la violence masculine"** |pinar|
====================================================================================================================================================================================================

Voir :ref:`pinar_selek_2024_10_19`
