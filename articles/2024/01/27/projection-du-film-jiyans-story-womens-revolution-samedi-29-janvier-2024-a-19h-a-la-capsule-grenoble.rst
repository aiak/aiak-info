.. index::
   pair: Film ; Jiyan’s Story : Women’s Revolution  (2024-01-27)

.. _aiak_2024_01_27:

=========================================================================================================================================================================================================================
♀️✊ ⚖️ **Femme, Vie, Liberté** 2024-01-27 Rejoignez-nous pour regarder et discuter du film **Jiyan’s Story : Women’s Revolution** samedi 27 Janvier 2024 à 19h à la Capsule (20 Rue Boucher de Perthes) Grenoble
=========================================================================================================================================================================================================================

- :ref:`iran_luttes:mahsa_jina_amini`
- https://kurdistan-au-feminin.fr/2024/01/27/grenoble-cine-discussion-sur-la-liberation-des-femmes-dans-les-resistances-kurdes/
- https://kurdistan-au-feminin.fr/2022/07/28/en-souvenir-de-jiyan-tolhildan/
- https://bellacaledonia.org.uk/2022/07/27/remembering-jiyan-tolhildan/
- https://vimeo.com/234408612


Annonces
===========

Annonce sur https://kurdistan-au-feminin.fr
----------------------------------------------------

- https://kurdistan-au-feminin.fr/2024/01/27/grenoble-cine-discussion-sur-la-liberation-des-femmes-dans-les-resistances-kurdes/


Annonce reçue
-------------------

.. figure:: images/affiche_2024_01_27.webp
   :width: 600


Annonce sur ici-grenoble
-------------------------------

.. figure:: images/infos_ici_grenoble.webp

   https://www.ici-grenoble.org/evenement/cine-discussion-jiyans-story-womens-revolution-sur-la-liberation-des-femmes-dans-les-resistances-kurdes


Le film
=========

https://vimeo.com/234408612

Présentation
----------------

Rejoignez-nous pour regarder et discuter du film « Jiyan’s Story : Women’s Revolution »
samedi 27 Janvier 2024 à 19h à la Capsule (20 Rue Boucher de Perthes) Grenoble

« Jiyan’s Story : Women’s Revolution » raconte l’histoire de la guérilla
Jiyan Tolhildan, qui a consacré 20 ans de sa vie à la lutte militante kurde.

A travers sa vie relatée dans le film se donne à voir le sens profond du
principe « Jin, Jiyan, Azadi » (Femme, Vie, Liberté) au cœur du paradigme
de libération du mouvement kurde.

**La résistance du peuple kurde est porteuse d’espoir au-delà des frontières
du Nord Est Syrien qu’elle a libéré par la lutte armée**.

Elle l’est toujours, et peut-être avec encore plus de force, alors que,
depuis début octobre, le gouvernement turc bombarde de nouveau la province
et ses infrastructures civiles.

Elle nous oblige et nous aide à penser les moyens de notre propre émancipation
du système capitaliste.

À la suite de la projection, nous aimerions donc, en nous inspirant de la
vision politique proposée par le mouvement kurde, discuter ensemble d’un
certain nombre de sujet :

- que signifie la libération des femmes dans le  mouvement kurde, et
  comment s’en emparer ?
- comment rester uni face à la montée des post-fascismes ?
- **comment redonner du sens à l’internationalisme depuis nos luttes locales
  et réussir à lutter ensemble au-delà des frontières tout en tenant compte
  des contextes singuliers ?**

La capsule 20 Rue Boucher de Perthes, Grenoble
=================================================

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.703658461570741%2C45.18221833171799%2C5.710739493370056%2C45.18529231034303&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=18/45.18376/5.70720">Afficher une carte plus grande</a></small>


En souvenir de Jiyan Tolhildan
=========================================

- https://kurdistan-au-feminin.fr/2022/07/28/en-souvenir-de-jiyan-tolhildan/

Alors que les drones turcs tuent impunément des femmes et hommes kurdes au
Rojava, la communauté internationale qui les applaudissait hier pour leur
bravoure face aux terroristes de DAECH se rend complice de leur meurtre en
détournant le regard, au lieu de stopper la Turquie criminelle.

L’une de ces femmes tombée martyre sous les bombes turques est la commandante
Jiyan Tolhildan. Elle était commandante des FDS en charge des opérations
conjointes avec la coalition internationale anti-EI.

Elle a été tuée avec deux autres combattantes des YPJ par un drone turc
près de Qamishlo le 22 juillet 2022.
La journaliste Sarah Glynn lui rend hommage dans son billet
intitulé « Remembering Jiyan Tolhildan (En souvenir de Jiyan Tolhildan) » .

En souvenir de Jiyan Tolhildan
-------------------------------------

Dimanche dernier, le Commandement central des États-Unis a présenté ses
condoléances à trois femmes combattantes – membres de leurs «partenaires des
FDS» qui ont été « tuées lors d’une attaque près de Qamishlo, en Syrie,
le 22 juillet 2022». Ils ont omis de mentionner que l’auteur de l’attaque
qui a assassiné les trois femmes était l’allié américain de l’OTAN,
la Turquie, et que le drone turc qui l’a perpétré a survolé l’espace
aérien contrôlé par les États-Unis.

La vie et la mort de Jiyan Tolhildan fournissent à la fois une inspiration
puissante et un avertissement effrayant. Vous pouvez la regarder raconter sa
propre histoire – et voir son sourire contagieux – dans un documentaire
tourné en 2015, après son rôle important dans la défense de Kobanê,
la bataille qui a renversé la vapeur contre l’EI.

Jiyan signifie vie et Tolhildan signifie vengeance – c’est-à-dire se
venger de l’oppression en construisant la révolution.

Le vrai nom de Jiyan était Salwa Yusuf. Elle est née en 1982 dans un village
d’Afrîn dans une famille traditionnelle kurde. Comme toute sa communauté,
elle a été rapidement sensibilisée à la discrimination anti-kurde, tant
de la part du régime syrien qui imposait l’enseignement arabe aux enfants
kurdes et les battait alors qu’ils se révoltaient, que des récits de sa
grand-mère sur le massacre de Dersim, perpétré en Turquie en 1938. Elle a
également pris conscience de l’oppression des femmes au sein de sa propre
communauté, où les filles étaient mariées jeunes et n’avaient pas leur
mot à dire dans leur vie. Et elle savait qu’il y avait un endroit dans les
montagnes où les femmes kurdes prenaient le contrôle de leur propre avenir :
où elles « s’instruisent et se battent pour leur honneur ». En 1998 (…)
elle quitte sa famille pour rejoindre le Parti des travailleurs du Kurdistan,
le PKK, où son frère était déjà allé se battre (et finalement tombé pour)
la libération kurde.



Tout d’abord, elles sont restées en Syrie, aidant d’autres filles à
échapper au mariage forcé. Puis, à l’âge de 18 ans, elle est partie dans
les bases montagneuses du PKK dans les monts Qandil, au nord de l’Irak, où les
guérilleros ont pu vivre selon leur propre modèle de société. Elle était
allée se battre pour une cause, mais a découvert qu’en plus d’apprendre
à se battre, elle avait également reçu une éducation sur la nature et
l’histoire de la société – et sur l’importance de retrouver un rôle
à part entière pour les femmes.

Lorsque les Kurdes du nord de la Syrie ont profité du vide de pouvoir laissé
par la guerre civile syrienne pour établir un contrôle autonome dans leur
région, les guérilleros syriens sont rentrés chez eux pour soutenir et
défendre la nouvelle société autonome. Jiyan a pu faire des retrouvailles
en larmes avec sa famille, mais elle a aussi donné toute son énergie à la
lutte et à la mise en place des unités de défense des femmes (YPJ). Les
YPJ, qui font désormais partie des Forces démocratiques syriennes (FDS), ont
joué un rôle essentiel dans la défense de Kobanê et la défaite de l’EI,
ainsi que dans la libération des femmes de la région. Au moment de sa mort,
Jiyan était cheffe des unités de lutte contre le terrorisme.

Le jour de son assassinat, elle avait participé à un forum sur les acquis
de dix ans de révolution des femmes au Rojava. Elle voyageait du forum avec
une autre, plus jeune, commandante des YPJ, Roj Khabur (Joana Hisso), et une
combattante des YPJ de dix-neuf ans, Barin Botan (Ruha Bashar), lorsque leur
voiture a été touchée par le drone turc et tous trois ont été tuées.

Lorsque les FDS étaient tout ce qui se dressait entre le reste du monde et
DAECH, les médias internationaux ne pouvaient pas se lasser de ces combattantes,
mais maintenant qu’elles sont attaquées par la Turquie, personne ne veut
le savoir. Le gouvernement turc est déterminé à éliminer tout vestige de
l’autodétermination kurde (…). Après avoir persuadé ses alliés aux
États-Unis et en Europe de qualifier le PKK de groupe terroriste, il est
furieux de voir ces pays travailler avec les FDS – même dans une alliance
purement tactique. (La démocratie radicale et l’idéologie anticapitaliste de
l’administration autonome garantissent que les puissances mondiales n’ont
aucune réelle sympathie d’intérêts.)

Les FDS n’ont pas les moyens de se protéger des airs et le contrôle de
l’espace aérien du nord de la Syrie est partagé entre les États-Unis et la
Russie. En 2018, la Russie a permis à la Turquie d’envahir et d’occuper le
canton occidental d’Afrîn ; et en 2019, le président Trump a retiré les
troupes américaines permettant à la Turquie d’occuper la bande de terre
entre Serê Kaniyê et Girê Spî. L’invasion de 2019 s’est terminée par
des cessez-le-feu négociés avec la Russie et les États-Unis, qui devaient se
porter garants. La Turquie rompt ces accords de cessez-le-feu tous les jours,
mais les garants n’ont rien fait. Pendant ce temps, les zones occupées sont
marquées par le nettoyage ethnique et la turquification. Leur administration
quotidienne a été confiée à des milices mercenaires qui se font concurrence
dans la terreur et l’extorsion et dans leurs interprétations brutales de
l’islam, tandis que les cellules de l’EI trouvent refuge.

En mai, le président Erdoğan a annoncé que la Turquie procéderait à une
troisième invasion de ce qui est maintenant connu sous le nom d’administration
autonome du nord et de l’est de la Syrie, et un grand nombre de soldats et
de véhicules militaires ont traversé la frontière entre la Turquie et la
Syrie occupée par la Turquie. Cette fois, ni la Russie ni les États-Unis ne
sont prêts à s’écarter du chemin d’Erdoğan – ses cibles préférées
sont dans la zone dominée par la Russie – mais il n’a montré aucun signe
de recul. S’il teste jusqu’où il peut aller sans réaction internationale
significative, il sera ravi. Outre le nombre croissant d’assassinats ciblés
de membres dirigeants des FDS, la Turquie a bombardé constamment des villages
le long de la ligne de front, tuant et mutilant des civils, détruisant des
maisons et des infrastructures et chassant délibérément les résidents locaux.

Les agressions étrangères d’Erdoğan sont largement interprétées comme
des tentatives de regagner le soutien populaire qu’il a perdu à cause de la
crise économique turque ; mais même si cela ne suffit pas à le maintenir au
pouvoir, la principale « opposition » politique n’a montré aucune volonté
de s’opposer à son nationalisme populaire. Le Parti républicain du peuple
(CHP) s’est joint au Parti de la justice et du développement (AKP) au pouvoir
pour condamner le fait que l’Amérique ait envoyé un message de condoléances.

La Turquie peut adopter cette position parce que le reste du monde les laisse
littéralement s’en tirer avec un meurtre. La géographie stratégique de la
Turquie lui permet de courtiser à la fois l’OTAN (dont elle est membre de
longue date) et la Russie. Personne ne veut risquer la colère de la Turquie,
donc jusqu’à ce qu’il y ait un fort mouvement les poussant d’en bas,
les politiciens continueront à détourner le regard et à ignorer les appels à
une zone d’exclusion aérienne. Et les gens qui ont tant fait, non seulement
pour vaincre DAECH, mais aussi pour donner l’espoir, par l’exemple,
d’une meilleure forme de société, ne peuvent s’attendre à aucune aide.

Chaque coup porté aux FDS est à la fois une tragédie personnelle et un
revers pour le progrès, mais il y a encore beaucoup de gens prêts à assumer
le fardeau de ceux qui sont tombés. Ils ont besoin de notre soutien, mais
leur esprit est inébranlable – comme Jiyan l’a expliqué dans le film :

« La mort est quelque chose de naturel pour nous. Elle est constamment dans
nos esprits et nos âmes. Où que vous alliez, les visages et les yeux des
martyrs sont toujours avec vous. Vous sentez que vous n’êtes pas seul. On
n’appelle pas ça la mort parce que les gens se sacrifient pour se construire
une nouvelle vie… Et nous partageons cette nouvelle vie, toujours souriants,
toujours rêvant de beauté. C’est le secret de la révolution. »

Les Kurdes disent que « les martyrs ne meurent jamais » ; mais, en plus de
maintenir vivants les idées et l’esprit de ceux qui ont fait ce sacrifice,
les gens partout dans le monde doivent s’unir pour faire pression sur les
dirigeants mondiaux afin qu’ils essaient de faire en sorte qu’il n’y
ait plus de martyrs.

L’histoire de Jiyan

Le film « Jiyan’s Story : Women’s Revolution » est déjà paru
en 2017. Il raconte la vie de Jiyan Tolhildan depuis son enfance dans un
village proche d’Efrîn jusqu’à la révolution des femmes qui a pris
place au Rojava/Nord-Est de la Syrie à partir de 2012. Contrairement à de
nombreux autres films sur la révolution et la lutte contre l’EI, celui-ci
ne se concentre ni sur la lutte militaire ni sur la seule lutte politique. Il
raconte toute l’histoire d’où les différentes luttes sont enracinées,
comment elles sont intégrées et pourquoi les femmes sont l’avant-garde de
la révolution.

En se concentrant sur un protagoniste, le réalisateur A. Halûk Ünal est
capable de mettre en lumière de nombreux aspects d’une vie de résistance
: Jiyan grandit à Efrîn près de la barrière frontalière qui sépare
le Kurdistan et déchire sa famille. À l’école, elle subit le racisme
du système éducatif arabe syrien. Elle se rebelle contre la séparation
des garçons et des filles dans le village et refuse de se marier contre son
gré. Fuyant sa famille et rejoignant le mouvement de libération, elle élargit
son horizon et apprend à connaître les autres groupes ethniques et religieux
de sa région. Sachant de première main comment les femmes chrétiennes et
musulmanes, les femmes éduquées et non éduquées partagent l’expérience
de l’oppression patriarcale, Jiyan réalise à quel point l’oppression
des femmes est centrale pour toutes les structures de pouvoir au Moyen-Orient.

Lorsque le printemps arabe commence en 2011, elle retourne dans les villes
pour aider à organiser le soulèvement pacifique contre l’État syrien
tout en organisant secrètement les femmes d’une manière que les hommes
ne remarqueraient pas : « Ils nous ont trompés pendant 5000 années. Notre
tromperie n’a même pas duré un an ».

Comme elle le dit, sans armes, vous ne pouvez pas aller loin si l’ennemi
est déterminé à utiliser la force. Ainsi, les YPG / YPJ se constituent,
repoussent les forces de l’État syrien sans coup férir et jouent ensuite
un rôle décisif dans la défaite de l’EI d’abord à Kobanê et plus
tard partout.

Le film montre tout cela, mais le véritable objectif est la lutte politique
pour changer les structures de la société et l’esprit des femmes et des
hommes. Le mouvement des femmes kurdes franchit une nouvelle étape vers
l’abolition du patriarcat en dispensant une éducation anti-patriarcale aux
hommes. Dans le film, on voit des jeunes hommes se vanter de combien ils ont
changé, tandis que d’autres baissent timidement les yeux.

« L’histoire de Jiyan » montre mieux que d’autres films les contradictions
de la société. Nous entendons des histoires déchirantes de femmes mariées
à l’âge de 12 ans. Ensuite, nous voyons les mêmes femmes s’émanciper
dans un environnement entièrement féminin au sein des YPJ.

Jiyan’s Story (en anglais) de Drama İstanbul Film Workshop sur Vimeo:
https://vimeo.com/234408612

Le texte à lire en anglais ici : `Remembering Jiyan Tolhildan <https://bellacaledonia.org.uk/2022/07/27/remembering-jiyan-tolhildan/>`_


Autre événement à Grenoble
==============================

- :ref:`iran_2024:iranluttes_2024_01_27`

