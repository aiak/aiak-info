
.. _hrant_dink_2024_01_19:

==========================================================================================================================================
**Le vendredi 19 janvier 2024 à 11h 30 Cérémonie mémorielle devant la plaque de rue Hrant Dink, à la confluence, Lyon 2e**
==========================================================================================================================================

Le vendredi 19 janvier 2024 à 11h 30 Cérémonie mémorielle devant la
plaque de rue Hrant Dink, à la confluence, Lyon 2e 

Invitée d’honneur: Madame Pinar Selek 

Historique
=============

Le 19 janvier 2007, à Istanbul, Hrant Dink journaliste directeur de publication
de l’hebdomadaire turco arménien AGOS était froidement assassiné devant
son journal, suscitant une vague d’émotion et de condamnation au sein de
la communauté arménienne, plus largement au sein des démocrates turcs et
de tous ceux qui sont attachés à la liberté de la Presse.  

Un an après, à Lyon, c’est au nom de cette liberté de la Presse, qu’à l’initiative de
la journaliste Jeanine Paloulian, avec le soutien du club de la Presse, de la
famille de Hrant Dink, de Reporters Sans Frontières et de l’ ISCPA et ses
étudiants en journalisme, le conseil municipal de Lyon décidait de donner
le nom de Hrant Dink à l’une des rues du quartier de la confluence. 

Elle était officiellement inaugurée par le maire de Lyon, Gérard Collomb, 
le jour anniversaire de cet assassinat. 

Depuis, chaque 19 janvier, une cérémonie a lieu, à la confluence, à la 
mémoire de Hrant Dink.  

En 2024 ce 17e triste anniversaire sera encore plus douloureux puisque 
son assassin, pourtant condamné à 23 ans de prison, vient d’être remis 
en liberté pour "bonne conduite". 

Le chaudron nationaliste turc est plus que jamais en ébullition.

Invitée d’honneur : Madame Pinar Selek 
=========================================

Madame Pinar Selek née en 1971, proche du journaliste Hrant Dink, sociologue,
écrivaine, docteur en sciences politiques, docteur Honoris Causa de l’ ENS
Lyon, Enseignante au département de Sociologie à l’Université de Nice
Côte d’Azur, 

Pinar Selek militante féministe, citoyenne d’honneur de la ville de Lyon, 
est l’auteure de plusieurs ouvrages dont : "Parce qu’il sont arméniens" 
traduit dans plusieurs langues. 

Elle vient de publier : Le chaudron militaire turc.

