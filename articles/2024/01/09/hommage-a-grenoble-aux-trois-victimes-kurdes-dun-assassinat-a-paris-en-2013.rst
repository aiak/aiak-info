.. index::
   pair: AIAK ; 2024-01-09

.. _aiak_2024_01_09:

==================================================================================================================
2024-01-09 **Hommage à Grenoble aux trois victimes kurdes d’un assassinat à Paris en 2013** par Edouard Schoene
==================================================================================================================

- https://travailleur-alpin.fr/2024/01/10/hommage-a-grenoble-aux-trois-victimes-kurdes-dun-assassinat-a-paris-en-2013/
- https://travailleur-alpin.fr/author/edouard-schoene/
- :ref:`assassinats_2013_01_09`


AIAK
=======

A l’appel de l’association AIAK (Association Iséroise des Amis des Kurdes)
se tenait mardi 9 janvier 2024 un rassemblement à Grenoble, dans le froid hivernal.


.. figure:: images/maryvonne_matheoud.webp

La coprésidente de l’association, Maryvonne Mathéoud a pris la parole
: « Dans la nuit du :ref:`9 au 10 janvier 2013 <assassinats_2013_01_09>`, les corps sans vie de trois
militantes kurdes sont retrouvés dans les bureaux du Centre d’Information
du Kurdistan à Paris. Elles ont été froidement abattues de balles dans la
tête. Les victimes ne sont pas des inconnues. Sakine Cansiz est l’une des
fondatrices du PKK avec A. Öcalan. Torturée dans les prisons de Diyarbakir,
elle apparaît comme une légende de la résistance et avait obtenu l’asile
politique en France. A ses côtés se trouvent Fidan Doğan et Leyla Söylemez.
…L’assassin, Ömer Güney, … a été missionné par les puissants services
secrets turcs (MIT) pour infiltrer la communauté kurde en région parisienne
et procéder à ces exécutions.  …Le Parquet antiterroriste est saisi et la
magistrate de ce pôle, ouvre une enquête pour « infraction terroriste ».
…Il n’y aura jamais de procès. Ömer Güney meurt le 17 décembre 2016
et amène la magistrate à procéder à l’extinction de l’action.


Le jour du crime, M. Valls, alors Premier ministre, déclarait devant les
médias que toute la vérité devait être faite sur ce crime. Onze jours plus
tard, M. Valls rencontrait en secret l’ambassadeur de Turquie et exprimait sa
volonté « d’améliorer les relations avec la Turquie . » Onze ans après
les faits, l’action en justice n’avance pas.

Une nouvelle agression meurtrière contre le communauté Kurde de France a eu
lieu le 23 décembre 2022 devant le siège du Conseil démocratique kurde de
France (CDKF) faisant trois morts et quatre blessés .

Lea cinquantaine de personnes présentes ont rendu un hommage aux victimes
et apporté leur soutien aux familles des victimes et à l’ensemble de la
communauté Kurde

.. figure:: images/aiak_et_jo_briant.webp


Le jour du crime, M. Valls, alors Premier ministre, déclarait devant les
médias que toute la vérité devait être faite sur ce crime. Onze jours plus
tard, M. Valls rencontrait en secret l’ambassadeur de Turquie et exprimait sa
volonté « d’améliorer les relations avec la Turquie . » Onze ans après
les faits, l’action en justice n’avance pas.

Une nouvelle agression meurtrière contre le communauté Kurde de France a eu
lieu le 23 décembre 2022 devant le siège du Conseil démocratique kurde de
France (CDKF) faisant trois morts et quatre blessés .

Lea cinquantaine de personnes présentes ont rendu un hommage aux victimes
et apporté leur soutien aux familles des victimes et à l’ensemble de la
communauté Kurde

UCL
======

La jeunesse était présente avec une présence remarquée de militants·es
de l’UCL (Union Communiste Libertaire, Grenoble), dont un des responsables
est intervenu : « Une délégation de dix militant.e.s de l’Union Communiste
Libertaire était présente lors de la manifestation du 6 janvier à Paris, (10
000 manifestants NDLR ) pour exiger la vérité sur le meurtre de Fidan, Sakîne,
Leyla, Mîr, Abdurrahman et Evin. Nous avons tenus au côté de l’organisation
communiste-libertaire allemande Die Platform un cortège rouge et noir au sein
d’un bloc internationaliste important. 

Nous exigeons que l’Europe retire le PKK de la liste des organisations
terroristes ! Le PKK résiste et se bat dans une lutte légitime pour
l’autodétermination, contre le fascisme, le colonialisme et l’impérialisme
de l’État turc.  Nous exigeons la libération d’Abdullah Öcalan, mais
aussi celle de Selahattin Demirtaş, et de toutes et tous les prisonniers
politiques !  Nous refusons la criminalisation et la répression que subissent
en France les mouvements de solidarité avec les peuples en lutte. »
