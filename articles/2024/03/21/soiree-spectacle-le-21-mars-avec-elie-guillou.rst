
.. _aiak_2024_03_21:

==================================================================================================================
2024-03-21 **Soirée autour de "l'homme tempéré" d'Elie Guillou le jeudi 21 mars à Antigone**
==================================================================================================================

Soirée autour de "l'homme tempéré" d'Elie Guillou
le jeudi 21 mars
à Antigone 21, rue des violettes 38 100 Grenoble

ouverture des portes à 19h, spectacle à 20h.

Déroulement
==================

- Présentation de la soirée
- Lien avec les soirées sur le même thème à Antigone
- Présentation de la situation et actualité kurde par AIAK

Spectacle puis discussion avec l'auteur
Un spectacle de lecture, récit et chants accompagnant la sortie du livre
'“L’homme tempéré” (Collection La belle étoile, Hachette livres.) suivi
d’une rencontre autour du livre, puis d’un échange autour de nos capacités
à composer avec notre volonté de lutter, notre intranquillité face aux
situations d’injustice et notre épuisement militant.


Résumé du livre
===================

En 2012, Élie Guillou se rend dans le sud-est de la Turquie afin d’y rencontrer
les dengbejs, des chanteurs traditionnels. Il y découvre la condition des Kurdes,
un peuple apatride dont la lutte est réprimée dans le sang.

Saisi, il cherche sa place au milieu de cette histoire brusque : dans les
manifestations massives, les camps de réfugiés, un studio de doublage de
dessins animés, sur la ligne de front syrienne...

Touriste, témoin, et puis après ?

À son retour en France, il peine à rendre compte de cette urgence lointaine.

Comment témoigner ? Et comment vivre sa paix quand la guerre existe ?

Dans ce récit d’apprentissage, un jeune homme issu d’un milieu tempéré
s’éveille à la part tragique du monde.

Il y raconte la colère face à l’indifférence, la honte face à l’impuissance.

Mais aussi la douleur à l’épreuve de la douceur.

A jeudi 21 mars
amitiés
Maryvonne Mathéoud
