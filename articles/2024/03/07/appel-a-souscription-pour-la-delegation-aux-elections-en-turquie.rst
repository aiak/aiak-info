
.. aiak_2024_03_07:

==================================================================================================================
2024-03-07 Appel à souscription pour la délégation aux élections en Truquie
==================================================================================================================

CherEs amiEs,

Le 31 mars 2024 auront lieu des élections locales (municipales et régionales)
en Turquie .

AIAK répond à la demande des militanEs kurdes et envoie une délégation pour
superviser ces élections

Nous faisons appel à vous pour financer cette délégation.
===============================================================

Nous avons besoin de la solidarité des groupes progressistes déclare Eyup Doru
dans un interview pour la CNSK (coordination Nationale solidarité kurdistan)

Eyup Doru représentant du HDP (Parti de la Démocratie du Peuple) en Europe
avant les présidentielles de mai 2023 est représentant du DEM (Parti de l’Egalité
et de la Démocratie des Peuples), nouveau parti créé pour contourner l'interdiction
imminente du HDP par le gouvernement d'Erdogan

Eyup Doru poursuit : notre projet politique pour cette région, ce projet que nous
appelons le Confédéralisme Démocratique n’est pas seulement un projet démocratique
pour les Kurdes, c’est un projet démocratique pour tous les peuples de la région.

Et par exemple au Rojava on voit la participation des arabes, des minorités
ethniques comme les arméniens ou les assyro-chaldéens.

Et je pense que c’est ce projet-là qui est attaqué principalement par tous
les Etats antidémocratiques, fascistes de la région.

Pour toutes ces raisons-là, nous avons besoin d’une solidarité internationale
de toutes les forces démocratiques progressistes.

Nous n’avons jamais participé « normalement » aux élections ! Il y a toujours
eu des arrestations de militants, il y a toujours eu des pressions politiques,
des fraudes électorales.

C'est pour ses raisons que notre association AIAK envoie une délégation pour
observer les élections et témoigner

AIAK ne peut à elle seule financer cette délégation qui pourrait être heureusement
nombreuse, avec plusieurs jeunes.
Nous ne pouvons faire supporter aux déléguéEs les frais de mission même si
certains· y contribuent en fonction de leurs moyens

Merci de répondre vite avec des dons, même modestes

Amitiés
Pour le bureau d'AIAK
Maryvonne Mathéoud
Ali Arslan

Danielle
================

Les personnes imposables bénéficient

d'une réduction d'impôts de 66% sur le montant des cotisations et des dons
dans la limite de 20% du revenu imposable annuel (loi fiscale en vigueur)

Règlement par chèque à l'ordre de "AIAK"
à l'adresse : AIAK chez Maryvonne Mathéoud 35, rue du Saint Eynard 38 600 Fontaine
ou par virement au compte ci dessous
IBAN : FR 1820041010170909476U02888
