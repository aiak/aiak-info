.. index::
   pair: Grenoble ; 7 janvier 2023
   pair: Hommage ; 7 janvier 2023

.. _hommage_aux_6_martyrs_kurdes_2023_01_07:

==================================================================================================================
2023-01-07 Hommage aux 6 camarades kurdes
==================================================================================================================

- :ref:`iran_luttes:iran_grenoble_2023_01_07`
- https://www.orion-hub.fr/w/9W8zrqrp6tQqF5AtQSQ4NR (vidéo)
- :ref:`iran_luttes:comptes_rendus_2023_01_07`


2022-12-23 |AbdurrahmanKizil| #AbdurrahmanKızıl |EmineKara| #EmineKara  |MirPerwer| #MirPerwer
================================================================================================================

- :ref:`kurdistan_luttes:assassinats_2022_12_23`


2013-01-09 **Triple assassinat de militantes kurdes #FidanDogan #SakineCansiz #LeylaSoylemez**
==================================================================================================

- :ref:`kurdistan_luttes:assassinats_2013_01_09`


Discours de Maryvonne Mathéoud présidente d’AIAK (Association iséroise des amis des Kurdes)
===========================================================================================

- https://travailleur-alpin.fr/2023/01/08/grenoble-a-repondu-present-a-lhommage-national-aux-victimes-kurdes-dassassinats/

.. figure:: images/zoreh_et_maryvonne.png
   :align: center

Zoreh, représentante de la communauté Iranienne à Grenoble a fait le point
sur la répression du régime dictatorial contre les manifestants mobilisés.

Deux nouvelles personnes ont été abattues par le régime après condamnation
à mort : :ref:`Mohammad Mahdi Karami <mehdi_karami>` et :ref:`Seyed Mohammad Hosseini <mohammad_hosseini>`.

Puis Maryvonne Mathéoud, présidente d’AIAK (Association iséroise des amis
des Kurdes) s’est exprimée


Incapable d’écraser la révolte, le régime iranien recourt aux exécutions.
Au moins 469 personnes, dont 63 enfants et 32 femmes, ont été tuées par
les forces du régime iranien dans les manifestations qui secouent l’Iran
depuis la mi-septembre 2022.

La répression féroce et meurtrière exercée par le régime iranien n’est pas
venue à bout des manifestations de colère en Iran depuis la mort,
le 16 septembre 2022, de la jeune kurde Jîna Mahsa Amini, décédée suite
à des tortures en garde à vue.

Parmi les quelque 19 000 manifestants arrêtés, des milliers se trouvent
encore aujourd’hui derrière les barreaux.
Ne parvenant pas à réprimer le mouvement de révolte par les arrestations
et la violence policière, le régime iranien a recours à la peine de mort
pour intimider les manifestants.

Au moins 30 manifestants ont été condamnés à la peine de mort jusqu’à présent.

Deux d’entre eux, Mohsen Shekari et Majid Reza Rehneverdi ont déjà été
exécutés. Les condamnés à mort sont accusés de « meurtre d’agents des
forces de l’État, inimitié envers Dieu, rébellion armée contre l’État
et insulte au prophète ».

AIAK apporte son soutien au mouvement populaire en Iran, au combat des
femmes pour leur liberté.

Nous soutenons le combat des femmes pour le droit à disposer de leur
corps et contre un régime patriarcal. Nous demandons la libération
immédiate et inconditionnelle de tous les détenus et l’arrêt immédiat
de la torture et de l’exécution des manifestants.

Il est important qu’il y ait un soutien large aux manifestantes et
manifestants iraniens.
Nous savons que les Kurdes sont particulièrement mobilisés dans le mouvement
populaire en Iran, et qu’ils subissent une répression très forte.
Nous sommes dans une situation où les Kurdes subissent une agression très
violente dans les quatre pays où ils se trouvent : Iran, Irak, Turquie, Syrie.
Les régimes turc et iranien savent très bien agir ensemble lorsque leurs
intérêts est en jeu.

Les Kurdes sont victimes de persécutions jusqu’en France, avec une certaine
complaisance du pouvoir français, qui ne protège pas les Kurdes et ne
montre pas beaucoup d’empressement à enquêter et trouver les coupables
lorsqu’il y a des agressions.

Un accord a été signé entre la France et la Turquie en 2011 pour notamment
« une coopération opérationnelle de lutte contre le terrorisme ».

Paris emprisonne et expulse les militants kurdes sous le prétexte qu’il
sont membres du PKK inscrit sur la liste des organisations terroriste.

**Nous demandons le retrait du PKK des organisations terroristes.**

Aujourd’hui nous commémorons l’assassinat de trois militantes kurdes,
assassinées dans la nuit du 9 au 10 janvier 2013. Deux semaines à peine
avant ce tragique anniversaire, les Kurdes doivent déplorer une agression
meurtrière et la mort de trois militants kurdes à Paris.

Il y aura 10 ans Fidan Doğan, Sakine Cansız et Leyla Söylemez étaient
lâchement exécutées dans la nuit du 9 au 10 janvier 2013 de Paris.

Ces crimes mettant en cause les services secrets Turc le MIT ne sont
toujours pas élucidés.
L’assassin présumé est mort quelques semaine avant le procès et l’affaire
est classée. Avec de nouveaux éléments une deuxième instruction est ouverte.

Mais sous couvert de la raison d’état les avocats n’ont pas accès aux
dossier classés du secret défense.

**Nous demandons que le secret-défense opposé à la justice par les services
secrets français soit levé** afin de connaître tous les commanditaires des
assassinats de 2013.


Le vendredi 23 décembre 2022, à Paris, alors que devait se tenir une
réunion de préparation de la commémoration de l’assassinat des 3 militantes
kurdes, dans le même arrondissement, un homme armé a abattu froidement
3 des personnes se trouvant à proximité des locaux du Centre Culturel
kurde Ahmet Kaya :

- Emine Kara, responsable du Mouvement des femmes kurdes en France,
- Mir Perwer, artiste kurde et Abdulrahman Kizil, « un citoyen kurde ordinaire »
  fréquentant quotidiennement l’association – car attaché à sa culture.

L’homme a également blessé trois autres personnes d’origine kurde, à
environ 150 mètres des premiers assassinats.

Dès l’annonce de cet attentat terroriste, le gouvernement français, par
l’intermédiaire du ministre de l’Intérieur et du président de la République,
se sont efforcés de marteler le message qu’il s’agissait « uniquement »
d’un acte raciste fomenté par un « raciste pathologique ».

Si le racisme et l’appartenance à l’extrême droite xénophobe du meurtrier
ne fait pas de doute compte tenu notamment de ses propres déclarations,
les pouvoirs publics doivent aussi et surtout s’interroger sur de troublantes
circonstances : si la réunion prévue n’avait pas été, la veille, repoussée
d’une heure, c’est un massacre bien plus grand visant de nombreux et
nombreuses responsables kurdes de France qui aurait eu lieu.

L’assassin n’a pas hésité à revenir sur ses pas pour achever d’une balle
dans la tête Emine Kara, principale responsable kurde présente à ce moment.

Enfin, entre les deux sites de tir, l’assassin semble délibérément s’être
attaqué à des boutiques occupés uniquement par des kurdes.

Aussi, nous nous interrogeons sur le caractère terroriste, en plus d’une
expression meurtrière du racisme.

Nous exigeons
----------------

Nous exigeons que :

- les assassinats du 23 décembre 2022 soient qualifiés d’attentat terroriste
  et que le parquet national spécialisé en matière de terrorisme soit saisi ;
- les commanditaires soient identifiés et jugés, sur le sol français,
  quel que soit leur niveau de responsabilité ;
- le statut de réfugié politique soit accordé sans difficultés aux Kurdes
  contraints à l’exil ;
- les lieux de rencontres de la communauté kurde ne soient pas surveillés
  mais protégés ;
- les mouvements prônant la haine, le racisme et la violence en ou hors
  de France soient dissous et leurs responsables jugés dans le cas où
  ils se seraient rendus coupables d’appel à la haine ou de troubles
  à l’ordre public.

Nous réaffirmons notre soutien et notre solidarité avec la communauté kurde
endeuillée pour la deuxième fois en moins de dix ans par ces actes odieux,
perpétrés en plein Paris.





