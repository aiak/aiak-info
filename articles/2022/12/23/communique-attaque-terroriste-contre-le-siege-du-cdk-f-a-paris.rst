.. index::
   pair: AIAK ; Vendredi 23 décembre 2022

.. _aiak_2022_12_23:

===========================================================================================================================================
Vendredi 23 décembre 2022 **Communiqué de l'AIAK concernant l'attaque terroriste contre le siège du CDK-F à Paris**
===========================================================================================================================================

- https://cdkf.fr/attaque-terroriste-contre-le-siege-du-cdk-f-a-paris/


Victimes de l'attentat terroriste visant les Kurdes à Paris
=============================================================

- 🥀Evîn Goyî
- 🥀Mîr Perwer
- 🥀Abdullah Kizil


Communiqué de l'AIAK
=======================

Attaque contre le CDKF

Bonjour,

AIAK apporte ses plus sincères condoléances aux victimes et à leurs proches
et demande à ce que justice soit faite face à ce terrible attentat.

Nous appelons à un rassemblement ce samedi 24 décembre 2022 à 14h00
rue Felix Poulat 3800 Grenoble

Ce rassemblement marquera la volonté de celles et ceux qui y participeront :

- de rendre hommage aux victimes de l’attentat de Paris le 23 décembre 2022
- d'apporter notre solidarité avec la communauté Kurde
- de demander aux autorités françaises de renforcer la protection de la
  communauté kurde et de combattre les agissements violents, notamment
  encouragées par les autorités gouvernementales turques

Pour aiak Maryvonne Matheoud et Ali Arslan co-présidents

Déclaration de la CNSK (**Coordination Nationale Solidarité Kurdistan**)
========================================================================

En pièce jointe vous trouverez la déclaration de la CNSK (Coordination Nationale
Solidarité Kurdistan) dont AIAK est membre, suite à l'attaque meurtrière
du 23 décembre à Paris faisant 3 morts et 3 blessés.

Nous appelons à un rassemblement ce samedi 24 décembre à 14h00 rue Felix Poulat

:download:`DECLARATION_CNSK_ATTAQUE_PARIS-2022_12_23.pdf`

Coordination Nationale Solidarité Kurdistan

- 16, rue d’Enghien 75010 Paris
- Tél : +33 6 45 41 76 68
- Email : cnskurd@gmail.com


Meurtrière agression contre le communauté Kurde de France

Samedi 23 décembre 2022 en fin de matinée une attaque meurtrière a eu
lieu à l’encontre de la communauté Kurde de France et a visé notamment
le Centre Démocratique Kurde en France (CDK-F).

Le bilan est lourd 3 personnes ont été tués et trois grièvement blessés.

La CNSK condamne cette odieuse agression qui a eu lieu à 15 jours du
10e anniversaire de l’assassinat à Paris, dans la nuit du 9 au 10 janvier 2013,
de trois militantes Kurdes.

Elle apporte son soutien aux familles des victimes et à l’ensemble de
la communauté Kurde de France.
Après les bombardements de l’armée turque sur des objectifs civils au
Kurdistan de Syrie, l’aggravation de la répression en Turquie et en Iran,
il n’est pas acceptable, ni tolérable que dans notre pays la sécurité
de femmes et hommes réfugié.e.s politiques du fait de la répression dont
ils sont victimes dans leur pays ne soit pas assurée.

Ce drame est à rapprocher des multiples agressions dont se sont rendu
responsables, sur le sol national, les fascistes turcs des Loups Gris
sans que les réponses judicaires et politiques appropriée n’aient été
apportée.

Toute la lumière doit être faite, par l’instruction en cours, pour
déterminer les conditions qui ont permis à l’auteur des faits d’agir
librement alors qu’il est sous le coup de poursuites judiciaires.

La CNSK appelle à la participation, Samedi 24 décembre 2022 aux côtés
des Kurdes, aux rassemblements qui auront lieu partout en France pour
dénoncer cette agression et apporter son soutien à la communauté kurde
de France:

- Amis du Peuple Kurde en Alsace
- Amitiés Corse Kurdistan
- Amitiés Kurdes de Bretagne (AKB)
- Amitiés Kurdes de Lyon Rhône Alpes
- Association Iséroise des Amis des Kurdes (AIAK)
- Association Solidarité France Kurdistan
- Centre d’Information du Kurdistan (CIK)
- Collectif Azadi Kurdistan Vendée (CAKV)
- Conseil Démocratique Kurde de France (CDK-F)
- Ensemble
- Mouvement de la Jeunesse Communiste de France
- Mouvement de la Paix
- Mouvement des Femmes Kurdes en France (TJK-F)
- MRAP (Mouvement contre le Racisme et pour l’Amitié́ entre les Peuples)
- Nouveau Parti Anticapitaliste (NPA)
- Parti Communiste Français (PCF)
- Réseau Sortir du Colonialisme
- Union Communiste Libertaire
- Union Démocratique Bretonne (UDB)
- Union Syndicale Solidaire
- Solidarité́ et Liberté́ Provence.
