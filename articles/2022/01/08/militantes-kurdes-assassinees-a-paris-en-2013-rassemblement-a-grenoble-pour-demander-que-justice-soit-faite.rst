.. index::
   pair: Joël Kermabon ; 2022-01-09
   AIAK; 2022-01-09

.. _manif_aiak_2022_01_09:

====================================================================================================================================================================================
Dimanche 9 janvier 2022 **Militantes kurdes assassinées à Paris en 2013 : rassemblement à Grenoble pour demander que justice soit faite** par © Joël Kermabon de place Gre'Net
====================================================================================================================================================================================

- https://www.placegrenet.fr/2022/01/09/militantes-kurdes-assassinees-a-paris-en-2013-rassemblement-a-grenoble-pour-demander-que-justice-soit-faite/556262
- |place_grenette| © Par Joël Kermabon


.. figure:: images/photo_des_3_militantes.png
   :align: center
   :width: 400

   Militantes kurdes assassinées à Paris en 2013


FOCUS
======

Répondant à l'appel de l’association iséroise des amis des Kurdes, une
trentaine de personnes se sont rassemblées, ce samedi 8 janvier 2022
rue Félix-Poulat à Grenoble, pour réclamer justice sur l'assassinat de
trois militantes kurdes à Paris le 9 janvier 2013.

Neuf ans après, les militants pro-kurdes restent déterminés pour que la
vérité éclate.
Ils exigent ainsi du gouvernement français la levée du secret-défense
qui bloque l'instruction de cette affaire, jamais totalement élucidée.

Sakine Cansiz, une des fondatrices du Parti des travailleurs du Kurdistan (PKK),
Fidan Dogan et Leyla Saylemez sont mortes le 9 janvier 2013, froidement
exécutées de plusieurs balles dans la tête en plein cœur de Paris.

Neuf ans après, ce samedi 8 janvier 2022, comme quasiment tous les ans
à cette date anniversaire, l'association iséroise des amis des Kurdes (Aiak)
avait appelé à un rassemblement pour réclamer justice dans cette affaire
jamais élucidée ni jugée.

Ainsi, une trentaine de personnes étaient-elles présentes rue Félix-Poulat
pour rendre hommage aux trois militantes kurdes assassinées et demander
que la vérité sur les circonstances de ce triple assassinat puisse
enfin éclater.

