.. index::
   pair: Joël Kermabon ; 2022-11-24
   AIAK; 2022-11-24

.. _manif_aiak_2022_11_24:

==========================================================================================================================================================================================
**La communauté kurde de Grenoble s’indigne des « crimes de guerre » de la Turquie après les bombardements en Irak et en Syrie** par  © Joël Kermabon de place Gre'Net
==========================================================================================================================================================================================

- https://www.placegrenet.fr/2022/11/24/la-communaute-kurde-de-grenoble-sindigne-des-crimes-de-guerre-de-la-turquie-apres-les-bombardements-des-kurdes-en-irak-et-en-syrie/585164
- |place_grenette| © Joël Kermabon -Place Gre'net

Préambule
============

REPORTAGE VIDÉO - À l'appel de l'Association iséroise des amis des Kurdes
(Aiak), une trentaine de personnes ont participé à un rassemblement
ce 23 novembre 2022 à 18 heures, rue Félix-Poulat à Grenoble.

Les manifestants ont dénoncé les « crimes de guerre » de la Turquie après
les bombardements qui ont frappé des villes du Nord et de l'Est de la
Syrie ces 19 et 20 novembre 2022, faisant de nombreuses victimes.


La pluie froide et battante qui se déversait sur Grenoble n'a pas découragé
les manifestants, rassemblés aux abords de l'église Saint-Louis à l'appel
de l'Association iséroise des amis des Kurdes (Aiak).

Ceux-ci voulaient ainsi exprimer leur indignation, suite à la frappe de
l'aviation turque sur au moins huit zones abritant des bases kurdes du
Parti des travailleurs du Kurdistan (PKK).

De surcroît, lundi 21 novembre 2022 avant l’aube, l’agence de presse
étatique irakienne INA avait fait état de raids iraniens, avec « des tirs
de missiles et des frappes de drones » contre « trois partis iraniens
d’opposition au Kurdistan » en Irak.

Pour le PKK, « ces opérations ne sont pas nouvelles. Elles durent sans
discontinuer depuis sept mois ». Période durant laquelle, « l'armée turque [a]
effectué 3 694 bombardements sur le sol du Kurdistan d'Irak », selon le parti.

La communauté kurde grenobloise a ainsi tenu à exprimer sa colère, et
appelle la communauté internationale et la France à intervenir de toute
urgence en faveur des Kurdes.

En effet, déplore l'Aiak, les raids de l’aviation turque ont déjà fait
une quinzaine de morts et de nombreux blessés dans les zones ciblées par
les bombardiers de Recep Tayyip Erdoğan, président de la République de Turquie.


Le Kurdistan iranien désormais « en situation de guerre »
==============================================================

« Dans le Kurdistan iranien, on bâillonne et on jette en prison les militants,
journalistes et artistes contre lesquels on prononce des peines de mort »,
fait par ailleurs savoir l'Aiak.

L'association accuse par ailleurs le pouvoir iranien, « confronté à des
manifestations depuis la mort le 16 septembre 2022 de la jeune Kurde
iranienne Mahsa Amini, arrêtée puis assassinée par la police des mœurs
à Téhéran », d’attiser les troubles dans le pays.

« Les gardiens de la Révolution tirent à balles réelles dans les villes,
alerte ainsi l'Aiak.
Et ce, à un moment où le régime terroriste iranien est incapable d’arrêter
les manifestations en cours au Kurdistan, désormais en situation de guerre ».

Selon l’Organisation iranienne des droits de l’Homme, les pasdarans1 ont
tué au moins 378 personnes, dont 43 enfants, depuis mi-septembre.

Mais, précise l'Aiak, « on estime que le nombre réel de morts est beaucoup
plus élevé. Des dizaines de milliers de personnes ont également été
arrêtées, tandis que six personnes sont condamnées à mort », dénonce-t-elle.


